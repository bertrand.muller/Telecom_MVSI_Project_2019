#include <stdio.h>
#include <stdlib.h>

int  main() {

    	int n;
	int i = 3;
	int count = 0;
	int c;
	int numbers[100];

	/*@ assert i == 3; */
	/*@ assert count == 0; */
    
    	printf("Enter the number of prime numbers required\n");
	scanf("%d", &n);
	    
	if ( n >= 1 ) {
		
		/*@ assert n >= 1; */
		printf("First %d prime numbers are :\n",n);
		printf("2\n");
		numbers[count] = 2;
    	}

	/*@ loop assigns count;
	    loop assigns i;
	    loop assigns c;
	    loop invariant count <= n;
	    loop variant n-count; */
    	for ( count = 2; count <= n ; ) {

		/*@ assert n >= count >= 2; */

		/*@ loop assigns c;
		    loop invariant 2 <= c <= i+1;
		    loop variant c; */
		for ( c = 2 ; c <= i-1 ; c++ ) {
			
			/*@ assigns \nothing;
			    breaks i%c == 0;
			    continues i%c != 0; */
		    	if ( i%c == 0 )
		        	break;;

			/*@ assert c <= i-1; */
		}
		if ( c == i ) {
			/*@ assert \forall integer k; 0 <= k <= count && numbers[k] <= \sqrt(i) ==> i%numbers[k] != 0; */
		    	printf("%d\n", i);
			numbers[count-1] = i;
			printf("%d\n", numbers[count-1]);
		    	count++;
			/*@ assert count <= n; */
		}
		
		i++;

		/*@ assert n > 0; */
		/*@ assert 3 <= i <= n; */
		/*@ assert c <= i-1; */
		/*@ assert 2 <= count <= n; */
    	}
	
	/*@ assert count-1 == n; */
	/*@ assert c == i; */

	/*@ assert \true; */
    	return 0;
}
