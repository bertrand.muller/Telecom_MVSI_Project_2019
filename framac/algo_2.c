#include <stdio.h>

int main() {
	
    	int array[100], n, c, d, position, swap;
    
    	printf("Enter number of elements\n");
    	scanf("%d", &n);
    
    	printf("Enter %d integers\n", n);

	/*@ ensures n < 100; */
    
	/*@ loop assigns *(array+(0..100)), c;
	    loop invariant 0 <= c < n;
	    loop variant n-c; */
	for ( c = 0 ; c < n ; c++ )
		/*@ assert rte: index_bound: 0 <= c <= n; */
	    	scanf("%d", &array[c]);
		/*@ assert \valid(&array[c]); */    

	/*@ loop assigns position, d, swap, *(array+(0..100)), c;
	    loop variant n-1-c; */
	for ( c = 0 ; c < (n-1) ; c++ ) {
		
        	position = c;

		/*@ assert position == c; */
    
		/*@ loop assigns d, *(array+(0..100)), swap;
		    loop invariant c+1 < d < n;
		    loop variant n-d; */
        	for ( d = c+1 ; d < n ; d++ ) {
			
			/*@ assert rte: index_bound: 0 <= position; */
            		if ( array[position] > array[d] )
				/*@ assert array[position] > array[d]; */
				position = d;
				/*@ assert (array[position] > array[d]) ==> (position == d); */
        		}
        		if ( position != c ) {
				/*@ assert position != c; */

           			swap = array[c];
				/*@ assert swap == array[c]; */

            			array[c] = array[position];
				/*@ assert array[c] == array[position]; */

            			array[position] = swap;
				/*@ assert array[position] == swap; */
        		}

			/*@ assert 0 < c < n-1; */
			/*@ assert c+1 <= d < n; */
    		}
    
    		printf("Sorted list in ascending order:\n");
    
	/*@ loop assigns c;
    	    loop invariant c < n;
    	    loop variant n-c; */
    	for ( c = 0 ; c < n ; c++ )
        	printf("%d\n", array[c]);

	/*@ assert \forall integer k; 0 < k < n ==> (array[k-1] <= array[k]); */
	/*@ assert 0 <= n < 100; */
	/*@ assert c == n; */
	/*@ assert d == n; */
    
	/*@ assert \true; */
    	return 0;
}
