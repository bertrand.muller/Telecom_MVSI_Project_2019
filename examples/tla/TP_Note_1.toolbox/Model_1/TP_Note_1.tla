----------------------------- MODULE TP_Note_1 -----------------------------
EXTENDS TLC, Integers
CONSTANTS S
     
(*
--algorithm sort {
variables s=S,temp,i,j,a;
{
    a[0]:=4; 
    a[1]:=2; 
    a[2]:=7; 
    a[3]:=1; 
    a[4]:=6;

    i:=s-2;
    while (i \geq 0){
        j:=0;
        while(j \leq i){
            if (a[j] > a[j+1]){
                temp:=a[j];
                a[j]:=a[j+1];
                a[j+1]:=temp;
            };
            j:=j+1;
        };        
        i:=i-1;
    };    
print <<"After">>;
print <<a>>;
}
}
*)
\* BEGIN TRANSLATION
CONSTANT defaultInitValue
VARIABLES s, temp, i, j, a, pc

vars == << s, temp, i, j, a, pc >>

Init == (* Global variables *)
        /\ s = S
        /\ temp = defaultInitValue
        /\ i = defaultInitValue
        /\ j = defaultInitValue
        /\ a = defaultInitValue
        /\ pc = "Lbl_1"

Lbl_1 == /\ pc = "Lbl_1"
         /\ a' = [a EXCEPT ![0] = 4]
         /\ pc' = "Lbl_2"
         /\ UNCHANGED << s, temp, i, j >>

Lbl_2 == /\ pc = "Lbl_2"
         /\ a' = [a EXCEPT ![1] = 2]
         /\ pc' = "Lbl_3"
         /\ UNCHANGED << s, temp, i, j >>

Lbl_3 == /\ pc = "Lbl_3"
         /\ a' = [a EXCEPT ![2] = 7]
         /\ pc' = "Lbl_4"
         /\ UNCHANGED << s, temp, i, j >>

Lbl_4 == /\ pc = "Lbl_4"
         /\ a' = [a EXCEPT ![3] = 1]
         /\ pc' = "Lbl_5"
         /\ UNCHANGED << s, temp, i, j >>

Lbl_5 == /\ pc = "Lbl_5"
         /\ a' = [a EXCEPT ![4] = 6]
         /\ i' = s-2
         /\ pc' = "Lbl_6"
         /\ UNCHANGED << s, temp, j >>

Lbl_6 == /\ pc = "Lbl_6"
         /\ IF i \geq 0
               THEN /\ j' = 0
                    /\ pc' = "Lbl_7"
               ELSE /\ PrintT(<<"After">>)
                    /\ PrintT(<<a>>)
                    /\ pc' = "Done"
                    /\ j' = j
         /\ UNCHANGED << s, temp, i, a >>

Lbl_7 == /\ pc = "Lbl_7"
         /\ IF j \leq i
               THEN /\ IF a[j] > a[j+1]
                          THEN /\ temp' = a[j]
                               /\ a' = [a EXCEPT ![j] = a[j+1]]
                               /\ pc' = "Lbl_8"
                          ELSE /\ pc' = "Lbl_9"
                               /\ UNCHANGED << temp, a >>
                    /\ i' = i
               ELSE /\ i' = i-1
                    /\ pc' = "Lbl_6"
                    /\ UNCHANGED << temp, a >>
         /\ UNCHANGED << s, j >>

Lbl_9 == /\ pc = "Lbl_9"
         /\ j' = j+1
         /\ pc' = "Lbl_7"
         /\ UNCHANGED << s, temp, i, a >>

Lbl_8 == /\ pc = "Lbl_8"
         /\ a' = [a EXCEPT ![j+1] = temp]
         /\ pc' = "Lbl_9"
         /\ UNCHANGED << s, temp, i, j >>

Next == Lbl_1 \/ Lbl_2 \/ Lbl_3 \/ Lbl_4 \/ Lbl_5 \/ Lbl_6 \/ Lbl_7
           \/ Lbl_9 \/ Lbl_8
           \/ (* Disjunct to prevent deadlock on termination *)
              (pc = "Done" /\ UNCHANGED vars)

Spec == Init /\ [][Next]_vars

Termination == <>(pc = "Done")

\* END TRANSLATION


=============================================================================
\* Modification History
\* Last modified Tue Dec 08 08:42:41 CET 2015 by garnier34u
\* Created Tue Dec 08 08:09:26 CET 2015 by garnier34u
