----------------------------- MODULE TP_Note_1 -----------------------------
EXTENDS TLC, Integers, Naturals
CONSTANTS s, Max, Min

A == [ p \in 0..s-1 |-> 
    IF p=0 THEN 3
    ELSE IF p=1 THEN 2
    ELSE IF p=2 THEN 5
    ELSE IF p=3 THEN 9
    ELSE IF p=4 THEN 7
    ELSE IF p=5 THEN 4
    ELSE 0
     ]
     
(*
--algorithm bubbleSort {
    variables i, j, temp,a=A;
{    
    i:=s-2;
    while(i \geq 0) {
        j:=0;
        while(j \leq i) {
            if(a[j] > a[j+1]) {
                temp:=a[j];
                a[j]:=a[j+1];
                a[j+1]:=temp;
            };
            j:=j+1;
        };
        i:=i-1;
    };
    print <<"Fin">>;
    print <<a>>;
}
}
*)
\* BEGIN TRANSLATION
CONSTANT defaultInitValue
VARIABLES i, j, temp, a, pc

vars == << i, j, temp, a, pc >>

Init == (* Global variables *)
        /\ i = defaultInitValue
        /\ j = defaultInitValue
        /\ temp = defaultInitValue
        /\ a = A
        /\ pc = "Lbl_1"

Lbl_1 == /\ pc = "Lbl_1"
         /\ i' = s-2
         /\ pc' = "Lbl_2"
         /\ UNCHANGED << j, temp, a >>

Lbl_2 == /\ pc = "Lbl_2"
         /\ IF i \geq 0
               THEN /\ j' = 0
                    /\ pc' = "Lbl_3"
               ELSE /\ PrintT(<<"Fin">>)
                    /\ PrintT(<<a>>)
                    /\ pc' = "Done"
                    /\ j' = j
         /\ UNCHANGED << i, temp, a >>

Lbl_3 == /\ pc = "Lbl_3"
         /\ IF j \leq i
               THEN /\ IF a[j] > a[j+1]
                          THEN /\ temp' = a[j]
                               /\ a' = [a EXCEPT ![j] = a[j+1]]
                               /\ pc' = "Lbl_4"
                          ELSE /\ pc' = "Lbl_5"
                               /\ UNCHANGED << temp, a >>
                    /\ i' = i
               ELSE /\ i' = i-1
                    /\ pc' = "Lbl_2"
                    /\ UNCHANGED << temp, a >>
         /\ j' = j

Lbl_5 == /\ pc = "Lbl_5"
         /\ j' = j+1
         /\ pc' = "Lbl_3"
         /\ UNCHANGED << i, temp, a >>

Lbl_4 == /\ pc = "Lbl_4"
         /\ a' = [a EXCEPT ![j+1] = temp]
         /\ pc' = "Lbl_5"
         /\ UNCHANGED << i, j, temp >>

Next == Lbl_1 \/ Lbl_2 \/ Lbl_3 \/ Lbl_5 \/ Lbl_4
           \/ (* Disjunct to prevent deadlock on termination *)
              (pc = "Done" /\ UNCHANGED vars)

Spec == Init /\ [][Next]_vars

Termination == <>(pc = "Done")

\* END TRANSLATION

Qcheck == pc="Done" => \A q \in 0..s-2: a[q] \leq a[q+1]
\* La condition Qcheck en tant que propri�t� dans le mod�le va v�rifier que si l'algorithme est termin�
\* alors chaque �l�ment dans "a" est plus petit que l'�l�ment suivant. Ainsi on est s�r que l'algorithme a fonctionn�

Qbounds == 
        /\ i \leq Max /\ Min \leq i
        /\ j \leq Max /\ Min \leq j
        /\ temp \leq Max /\ Min \leq temp
        /\ \A q \in 0..s-1: a[q] \leq Max /\ a[q] \geq Min

\*On pose Qbounds en tant qu'invariant ce qui permet de v�rifier que les variables de l'algorithmes sont toujours
\*entre les Min et Max d�finis
        

=============================================================================
\* Modification History
\* Last modified Tue Dec 08 09:14:03 CET 2015 by garnier34u
\* Created Tue Dec 08 08:09:26 CET 2015 by garnier34u
