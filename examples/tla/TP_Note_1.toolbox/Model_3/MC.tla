---- MODULE MC ----
EXTENDS TP_Note_1, TLC

\* CONSTANT definitions @modelParameterConstants:0s
const_1449562448369253000 == 
6
----

\* CONSTANT definitions @modelParameterConstants:1defaultInitValue
const_1449562448379254000 == 
0
----

\* CONSTANT definitions @modelParameterConstants:2Min
const_1449562448389255000 == 
-256
----

\* CONSTANT definitions @modelParameterConstants:3Max
const_1449562448399256000 == 
255
----

\* SPECIFICATION definition @modelBehaviorSpec:0
spec_1449562448409257000 ==
Spec
----
\* INVARIANT definition @modelCorrectnessInvariants:0
inv_1449562448419258000 ==
Qbounds
----
\* PROPERTY definition @modelCorrectnessProperties:0
prop_1449562448429259000 ==
Termination
----
\* PROPERTY definition @modelCorrectnessProperties:1
prop_1449562448439260000 ==
Qcheck
----
=============================================================================
\* Modification History
\* Created Tue Dec 08 09:14:08 CET 2015 by garnier34u
