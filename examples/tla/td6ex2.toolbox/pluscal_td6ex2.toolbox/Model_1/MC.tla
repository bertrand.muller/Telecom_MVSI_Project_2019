---- MODULE MC ----
EXTENDS pluscal_td6ex2, TLC

\* CONSTANT definitions @modelParameterConstants:1x1
const_154384729346036000 == 
0
----

\* CONSTANT definitions @modelParameterConstants:2x2
const_154384729346037000 == 
5
----

\* CONSTANT definitions @modelParameterConstants:3MMAX
const_154384729346038000 == 
45900
----

\* CONSTANT definitions @modelParameterConstants:4MMIN
const_154384729346039000 == 
-45000
----

\* SPECIFICATION definition @modelBehaviorSpec:0
spec_154384729346040000 ==
Spec
----
\* INVARIANT definition @modelCorrectnessInvariants:0
inv_154384729346041000 ==
SAFERTE
----
=============================================================================
\* Modification History
\* Created Mon Dec 03 15:28:13 CET 2018 by muller337u
