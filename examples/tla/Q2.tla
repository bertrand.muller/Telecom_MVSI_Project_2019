------------------------------ MODULE Q2 ------------------------------
(* Modules de base importables *)
EXTENDS Naturals, TLC
---------------------------------------------
CONSTANTS a,b
---------------------------------------------
VARIABLES x,y,z
---------------------------------------------

(* Calcul du PGCD *)
A == x<y /\ y'=y-x /\ x'=x /\ z'=z
B == x>y /\ x'=x-y /\ y'=y /\ z'=z
C == x=y /\ z'=x /\ x'=x /\ y'=y

Next == A \/ B \/ C
Init == x=a /\ y=b /\ z=0

question == x#y

=============================================