--------------- MODULE fichier --------------
(* Modules de base importables *)
EXTENDS Naturals, Integers, TLC
---------------------------------------------
CONSTANTS min, max
--------------------------------------------

(*
--algorithm division {
    variables u = 5;
              v = u+7;
    {
        l1: u:=u+v+5;
        l2: v:=v+u;
        l3: skip;
    }
}
*)
\* BEGIN TRANSLATION
VARIABLES u, v, pc

vars == << u, v, pc >>

Init == (* Global variables *)
        /\ u = 5
        /\ v = u+7
        /\ pc = "l1"

l1 == /\ pc = "l1"
      /\ u' = u+v+5
      /\ pc' = "l2"
      /\ v' = v

l2 == /\ pc = "l2"
      /\ v' = v+u
      /\ pc' = "l3"
      /\ u' = u

l3 == /\ pc = "l3"
      /\ TRUE
      /\ pc' = "Done"
      /\ UNCHANGED << u, v >>

Next == l1 \/ l2 \/ l3
           \/ (* Disjunct to prevent deadlock on termination *)
              (pc = "Done" /\ UNCHANGED vars)

Spec == Init /\ [][Next]_vars

Termination == <>(pc = "Done")

BF(X) == X \in min..max
Safety == BF(u) /\ BF(v)

\* END TRANSLATION

=============================================


