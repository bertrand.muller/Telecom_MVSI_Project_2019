---- MODULE MC ----
EXTENDS fichier, TLC

\* CONSTANT definitions @modelParameterConstants:0max
const_154748899006379000 == 
1000
----

\* CONSTANT definitions @modelParameterConstants:1min
const_154748899006380000 == 
-1000
----

\* INIT definition @modelBehaviorInit:0
init_154748899006381000 ==
Init
----
\* NEXT definition @modelBehaviorNext:0
next_154748899006382000 ==
Next
----
\* INVARIANT definition @modelCorrectnessInvariants:0
inv_154748899006383000 ==
Safety
----
=============================================================================
\* Modification History
\* Created Mon Jan 14 19:03:10 CET 2019 by muller337u
