----------------------------- MODULE TP_Note_2 -----------------------------
EXTENDS TLC, Integers, Naturals
CONSTANTS n

r == [ p \in 0..n-1 |-> 
    IF p=0 THEN 3
    ELSE IF p=1 THEN 20
    ELSE IF p=2 THEN 19
    ELSE IF p=3 THEN 53
    ELSE IF p=4 THEN 23
    ELSE IF p=5 THEN 99
    ELSE 0
     ]

(*
--algorithm disk {
variables c=[k \in 0..n-1 |-> 0],d,i,j,p;

process (D=0){
l0:i:=0;
l1:while (i<n){
cal:    c[i]:=<<i,r[i]*r[i]*3>>;
    i:=i+1;
    };
};

process (T=1) {
    deb:j:=0;
    parc:while (j < n) {
    sync1: await i > 0;
    sync:await i \geq j;
    if (c[j][2] < 10) {
       d[0]:=d[0]+1;       
    } else if (c[j][2] \leq 50) {
       d[1]:=d[1]+1; 
    } else {
       d[2]:=d[2]+1; 
    };
    c[j]:=0;
    j := j+1;
};
}

process (M=2){
syncD:await i=n;
syncT:await j=n;

print <<"End">>;

}



} \* fin disk


}
*)
\* BEGIN TRANSLATION
CONSTANT defaultInitValue
VARIABLES c, d, i, j, p, pc

vars == << c, d, i, j, p, pc >>

ProcSet == {0} \cup {1} \cup {2}

Init == (* Global variables *)
        /\ c = [k \in 0..n-1 |-> 0]
        /\ d = defaultInitValue
        /\ i = defaultInitValue
        /\ j = defaultInitValue
        /\ p = defaultInitValue
        /\ pc = [self \in ProcSet |-> CASE self = 0 -> "l0"
                                        [] self = 1 -> "deb"
                                        [] self = 2 -> "syncD"]

l0 == /\ pc[0] = "l0"
      /\ i' = 0
      /\ pc' = [pc EXCEPT ![0] = "l1"]
      /\ UNCHANGED << c, d, j, p >>

l1 == /\ pc[0] = "l1"
      /\ IF i<n
            THEN /\ pc' = [pc EXCEPT ![0] = "cal"]
            ELSE /\ pc' = [pc EXCEPT ![0] = "Done"]
      /\ UNCHANGED << c, d, i, j, p >>

cal == /\ pc[0] = "cal"
       /\ c' = [c EXCEPT ![i] = <<i,r[i]*r[i]*3>>]
       /\ i' = i+1
       /\ pc' = [pc EXCEPT ![0] = "l1"]
       /\ UNCHANGED << d, j, p >>

D == l0 \/ l1 \/ cal

deb == /\ pc[1] = "deb"
       /\ j' = 0
       /\ pc' = [pc EXCEPT ![1] = "parc"]
       /\ UNCHANGED << c, d, i, p >>

parc == /\ pc[1] = "parc"
        /\ IF j < n
              THEN /\ pc' = [pc EXCEPT ![1] = "sync1"]
              ELSE /\ pc' = [pc EXCEPT ![1] = "Done"]
        /\ UNCHANGED << c, d, i, j, p >>

sync1 == /\ pc[1] = "sync1"
         /\ i > 0
         /\ pc' = [pc EXCEPT ![1] = "sync"]
         /\ UNCHANGED << c, d, i, j, p >>

sync == /\ pc[1] = "sync"
        /\ i \geq j
        /\ IF c[j][2] < 10
              THEN /\ d' = [d EXCEPT ![0] = d[0]+1]
              ELSE /\ IF c[j][2] \leq 50
                         THEN /\ d' = [d EXCEPT ![1] = d[1]+1]
                         ELSE /\ d' = [d EXCEPT ![2] = d[2]+1]
        /\ c' = [c EXCEPT ![j] = 0]
        /\ j' = j+1
        /\ pc' = [pc EXCEPT ![1] = "parc"]
        /\ UNCHANGED << i, p >>

T == deb \/ parc \/ sync1 \/ sync

syncD == /\ pc[2] = "syncD"
         /\ i=n
         /\ pc' = [pc EXCEPT ![2] = "syncT"]
         /\ UNCHANGED << c, d, i, j, p >>

syncT == /\ pc[2] = "syncT"
         /\ j=n
         /\ PrintT(<<"End">>)
         /\ pc' = [pc EXCEPT ![2] = "Done"]
         /\ UNCHANGED << c, d, i, j, p >>

M == syncD \/ syncT

Next == D \/ T \/ M
           \/ (* Disjunct to prevent deadlock on termination *)
              ((\A self \in ProcSet: pc[self] = "Done") /\ UNCHANGED vars)

Spec == Init /\ [][Next]_vars

Termination == <>(\A self \in ProcSet: pc[self] = "Done")

\* END TRANSLATION

=============================================================================
\* Modification History
\* Last modified Tue Dec 08 09:53:13 CET 2015 by garnier34u
\* Created Tue Dec 08 09:15:43 CET 2015 by garnier34u
