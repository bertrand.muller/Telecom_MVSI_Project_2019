---- MODULE MC ----
EXTENDS Q4, TLC

\* CONSTANT definitions @modelParameterConstants:0min
const_153684998254674000 == 
-32768
----

\* CONSTANT definitions @modelParameterConstants:1NIMPORTEQUOI
const_153684998254775000 == 
32768
----

\* CONSTANT definitions @modelParameterConstants:2x1
const_153684998254776000 == 
33
----

\* CONSTANT definitions @modelParameterConstants:3x2
const_153684998254777000 == 
5
----

\* CONSTANT definitions @modelParameterConstants:4max
const_153684998254778000 == 
32767
----

\* INIT definition @modelBehaviorInit:0
init_153684998254779000 ==
Init
----
\* NEXT definition @modelBehaviorNext:0
next_153684998254780000 ==
Next
----
\* INVARIANT definition @modelCorrectnessInvariants:0
inv_153684998254781000 ==
safety1
----
\* INVARIANT definition @modelCorrectnessInvariants:1
inv_153684998254782000 ==
safety2
----
=============================================================================
\* Modification History
\* Created Thu Sep 13 16:46:22 CEST 2018 by muller337u
