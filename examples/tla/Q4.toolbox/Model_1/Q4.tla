------------------------------ MODULE Q4 ------------------------------
(* Modules de base importables *)
EXTENDS Integers,Naturals, TLC
---------------------------------------------
CONSTANTS NIMPORTEQUOI,x1,x2,min,max
---------------------------------------------
VARIABLES y1,y2,y3,c,z1,z2
---------------------------------------------

labels == {"START","LOOP","HALT"}

Init == /\ c = "START" 
        /\ y1 = NIMPORTEQUOI
        /\ y2 = NIMPORTEQUOI
        /\ y3 = NIMPORTEQUOI
        /\ z1 = NIMPORTEQUOI
        /\ z2 = NIMPORTEQUOI

A == /\ c = "START"
     /\ y1' = 0
     /\ y2' = 0
     /\ y3' = x1
     /\ UNCHANGED<<z1,z2>>
     /\ c' = "LOOP"
     
B == /\ c = "LOOP" /\ y3 = 0
     /\ z1' = y1
     /\ z2' = y2
     /\ UNCHANGED<<y1,y2,y3>>
     /\ c' = "HALT"
     
C == /\ c = "LOOP" /\ y3 # 0
     /\ y1' = IF y2 + 1 = x2 THEN y1 + 1 ELSE y1
     /\ y2' = IF y2 + 1 = x2 THEN 0 ELSE y2 + 1
     /\ y3' = y3 - 1
     /\ UNCHANGED<<c,z1,z2>>
     
Over == c = "HALT" /\ UNCHANGED<<c,y1,y2,y3,z1,z2>>     
     
Next == A \/ B \/ C \/ Over
     
safety1 == c = "HALT" => /\ 0 \leq z2
                         /\ z2 < x2
                         /\ x1 = z1*x2+z2
                         
safety2 == /\ (y1 # NIMPORTEQUOI => y1 \in min..max)
           /\ (y2 # NIMPORTEQUOI => y2 \in min..max)
           /\ (y3 # NIMPORTEQUOI => y3 \in min..max)
           /\ (z1 # NIMPORTEQUOI => z1 \in min..max)
           /\ (z2 # NIMPORTEQUOI => z2 \in min..max)
                              

=============================================