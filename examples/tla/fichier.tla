------------------------------ MODULE fichier --------------
(* Modules de base importables *)
EXTENDS Naturals, TLC
---------------------------------------------
CONSTANTS max
---------------------------------------------
VARIABLES np
---------------------------------------------

(* tentative 1 *)
entrer == np'=np+1
sortir == np'=np-1
Next == entrer \/ sortir
Init == np=0
Q1 == np#789 (* # == différent *)

---------------------------------------------

(* Exemple opérateurs logiques *)
(*
    f1 \/ f2 ((f3 /\ f4) \/ f5)
    
    équivaut à
    
    \/ f1
    \/ f2
    \/ \/ /\ f3
          /\ f4
       \/ f5   
*)

---------------------------------------------

(* tentative 2 *)
entrer2 == np<max /\ np'=np+1
Next2 == entrer2 \/ sortir

---------------------------------------------

(* tentative 3 *)
sortir2 == np>0 /\ np'=np-1
Next3 == entrer2 \/ sortir2

---------------------------------------------

(* propriétés à vérifier à chaque "next" exécuté *)
safety1 == np \leq max
safety2 == 0 \leq np
question1 == np#6

=============================================