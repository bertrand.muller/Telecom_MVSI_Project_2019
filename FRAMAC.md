# Télécom Nancy MVSI 2019 - _Frama-C Commands_

#### Analyze file and open result in GUI

```sh
frama-c-gui -wp -wp-invariants -wp-print -rte src/short.c
```

