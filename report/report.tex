\documentclass{tnreport}
%\documentclass[stage2a]{tnreport1} % If you are in 2nd year
%\documentclass[confidential]{tnreport1} % If you are writing confidential report

\def\reportTitle{Modélisation et vérification des systèmes informatiques} % Titre du mémoire
\def\reportAuthor{Ophélien Amsler \& Bertrand Müller}

\usepackage{lipsum}
\usepackage{subcaption}
\usepackage{adjustbox}
\usepackage{dirtree}
\usepackage{tikz}
\usepackage{multirow}
\usetikzlibrary{trees}
\usepackage{tikz-qtree}
\usepackage{makecell}
\usepackage{hhline}
\usepackage{colortbl}
\usepackage[most]{tcolorbox}
\usepackage{pdfpages}
\usepackage{draftwatermark}
\usepackage[stable]{footmisc}
\usepackage{hhline}
\usepackage{float}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{listings}

\begin{document}

\maketitle

\clearpage

\renewcommand{\baselinestretch}{0.5}\normalsize
\tableofcontents
\renewcommand{\baselinestretch}{1.0}\normalsize

\clearpage

\chapter{Introduction}

Dans le cadre de ce projet, on souhaite vérifier que deux programmes écrits en langage C renvoient le résultat attendu et qu'aucune erreur à l'exécution ne se produit. Pour cela, nous utilisons trois logiciels : \textbf{TLA+ Toolbox} avec le langage \textbf{TLA}, \textbf{Rodin} avec le langage \textbf{Event-B} et \textbf{Frama-C} avec le langage \textbf{ASCL}. 

Afin d'illustrer notre démarche, ce rapport est découpé en plusieurs parties. La première d'entre elles vise à présenter les algorithmes donnés avant analyse. Une partie supplémentaire présente les préconditions et les postconditions nécessaires à l'analyse de chacun des algorithmes.

Enfin, la dernière partie présente les difficultés rencontrées au moment de l'analyse des algorithmes ou de l'utilisation des logiciels. 

\chapter{Présentation des algorithmes}

\section{Algorithme n°1}

\subsection{Code}

\begin{figure}[ht]
	\lstinputlisting[language=C]{../src/algo_1_programme_premier.c}
	\caption{Code du premier programme en langage C}
\end{figure}

\subsection{Présentation}

Dans ce premier programme, on souhaite afficher à l'utilisateur les \textbf{\textit{n}} premiers nombres premiers. Le nombre \textbf{\textit{n}} est renseigné par l'utilisateur au moment de l'exécution du programme. La figure \ref{fig:exemple_execution_algo_1} correspond à un exemple d'exécution de ce programme avec \textbf{\textit{n}} = 7. On peut alors visualiser la liste des sept premiers nombres premiers.  

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\includegraphics[scale=0.8]{figures/gcc_algo_1}
	\caption{Exemple d'exécution avec \textit{\textbf{n}} = 7 pour l'algorithme n°1}
	\label{fig:exemple_execution_algo_1}
\end{figure}

\subsection{Précondition}

Le nombre \textbf{n} saisi est un entier naturel : $n \in \mathbb{N}$.

\subsection{Postcondition}

\begin{enumerate}
	\item Le nombre de nombres premiers déterminés doit être égal au nombre de nombres premiers désiré : \textbf{$count = n$}.
	
	\bigskip
	
	\item Chaque nombre généré doit être un nombre premier. Il faut vérifier que chaque nombre n'est pas divisible par aucun nombre premier inférieur ou égal à sa racine carrée.
\end{enumerate}

\subsection{Invariants}

\begin{itemize}[label=\textbullet]
	\item $n \in \mathbb{N}$
	
	\medskip
	
	\item $i \in \mathbb{N}$ \\
			$3 \leq i \leq n$
	
	\medskip
	
	\item $c \in \mathbb{N}$ \\
			$c \leq i-1$ 
	
	\medskip
	
	\item $count \in \mathbb{N}$ \\
			$2 \leq count \leq n$
\end{itemize}

\section{Résultat}

A la fin de ce projet, nous avons réussi à valider le programme et à vérifier l'absence d'erreurs à l'exécution sur les trois plateformes. Nous pouvons donc affirmer que les préconditions et postconditions présentées ci-dessus sont vérifiées. La liste ci-dessous regroupe les fichiers correspondants pour chacune des plateformes : 

Fichier \textbf{TLA} : 		./tla/algo\_1.tla \\
Fichier \textbf{Frama-C} : 	./framac/algo\_1.c \\
Fichier \textbf{Rodin} : 	./rodin/Algo\_1/*

\clearpage

\section{Algorithme n°2}

\subsection{Code}

\begin{figure}[ht]
	\lstinputlisting[language=C]{../src/algo_2_tri_selection.c}
	\caption{Code du second programme en langage C}
\end{figure}

\subsection{Présentation}

Ce second programme vise à afficher les \textbf{n} nombres entrés dans l'ordre croissant. Les \textbf{n} nombres sont renseignés par l'utilisateur au moment de l'exécution du programme. La figure \ref{fig:exemple_execution_algo_2} correspond à un exemple d'exécution de ce programme avec \textbf{n} = 7. On peut alors voir que les 7 nombres ont été triés à la sortie du programme.  

\begin{figure}[H]
	\centering
	\fboxsep=1.2pt
	\includegraphics[scale=0.45]{figures/gcc_algo_2}
	\caption{Exemple d'exécution avec \textit{\textbf{n}} = 7 pour l'algorithme n°2}
	\label{fig:exemple_execution_algo_2}
\end{figure}

\subsection{Précondition}

Le nombre \textbf{n} saisi est un entier naturel : $n \in \mathbb{N}$.

\subsection{Postcondition}

\begin{enumerate}
	\item Le nombre de nombres triés doit être égal au nombre de nombres entrés : $c = n$.
	\item Chaque nombre du tableau (affiché en fin de programme) doit être inférieur au nombre qui lui succède dans ce même tableau. \\
	$\forall i \in 1..n-1, array[i-1] \leq array[i]$ 
\end{enumerate}

\subsection{Invariants}

\begin{itemize}[label=\textbullet]
	\item $n \in \mathbb{N}$
	
	\item $c \in \mathbb{N}$ \\
	$0 < c < n-1$
	
	\item $d \in \mathbb{N}$ \\
	$c+1 < d < n$ 
	
	\item $position \in \mathbb{N}$
\end{itemize}

\section{Résultat}

A la fin de ce projet, nous avons réussi à valider le programme et à vérifier l'absence d'erreurs à l'exécution sur les trois plateformes. Nous pouvons donc affirmer que les préconditions et postconditions présentées ci-dessus sont vérifiées. La liste ci-dessous regroupe les fichiers correspondants pour chacune des plateformes : 

Fichier \textbf{TLA} : 		./tla/algo\_2.tla \\
Fichier \textbf{Frama-C} : 	./framac/algo\_2.c \\
Fichier \textbf{Rodin} : 	./rodin/Algo\_2/*

\chapter{Difficultés rencontrées}

\section{TLA}

Concernant TLA+ Toolbox, nous n'avons pas eu de difficulté particulière. Nous avons réussi à prouver nos programmes grâce à la syntaxe PlusCal.

\section{Frama-C}

\subsection{Modification de code}

Lors la première utilisation de Frama-C, nous avons constaté que le code source avait été modifié par le logiciel. En effet, Frama-C réalise des modifications de code pour améliorer la lisibilité et vérifier les conditions ASCL plus facilement. Il a donc été difficile de faire coïncider nos souhaits de preuves et les modifications apportées par Frama-C, notamment au niveau de la transformation des boucles "for" en boucles "while". Les commentaires ASCL étaient donc difficiles à placer dans le code source en raison des changements apportés par Frama-C. 

\subsection{Messages d'erreur}

Lors de nos multiples analyses, nous avons été confrontés à un problème assez important. En effet, nous avons constaté qu'une erreur unique est affichée pour des problèmes strictement différents à la base ("user input error"). De plus, lorsqu'une condition n'est pas vérifiée, Frama-C est généralement dans l'impossibilité de nous en indiquer la raison. Il faut donc retourner dans le code source et essayer de trouver une solution de manière empirique. Nous avons donc perdu beaucoup de temps pour trouver une solution à chacun des problèmes et nous n'avons malheureusement pas pu fournir des preuves complètement opérationnelles pour les programmes donnés.

\subsection{Installation de plugins}

Lors de nos recherches pour créer des conditions en ASCL, nous avons constaté qu'il était possible d'ajouter des plugins à Frama-C pour permettre leur vérification. Cependant, nous n'avons jamais réussi à faire en sorte que Frama-C ait connaissance de ces plugins. Par exemple, nous souhaitions utiliser le plugin Jessie pour la vérification déductive de programmes et vérifier la validité de nos préconditions et postconditions. L'installation avec succès de plugins nous aurait permis de vérifier plus efficacement les codes sources en C. 

\subsection{Initialisation des variables}

Dans les programmes donnés, certaines variables n'ont pas été initialisées avec une valeur par défaut. Ainsi, Frama-C n'était pas en mesure de vérifier certaines conditions écrites en ASCL. Pour résoudre ce problème, nous avons dû ajouter des valeurs par défaut à chacune de ces variables.

\subsection{Documentation}

La dernière contrainte rencontrée réside dans la difficulté à parcourir la documentation pour trouver des solutions à nos problèmes. En effet, la documentation nous est apparue comme longue et complexe avec une multitude de syntaxes différentes pour un même problème. Nous avons donc perdu beaucoup de temps à localiser les informations utiles à la réalisation des preuves de programmes. 

\section{Rodin}

\subsection{Installation}

La première difficulté que nous avons rencontrée est l'installation du logiciel sur nos propres machines. Nous avions des machines basées sur Unix mais nous avons été amenés à installer un grand nombre de dépendances. Nous avons finalement réussi à l'installer sur une de nos machines avec une distribution Debian. 

\subsection{Prise en main du logiciel}

Par la suite, nous avons eu des difficultés à le prendre main pour générer nos preuves de programme et pour comprendre la manière dont ces dernières sont vérifiées. Nous avons donc tenté de définir les différentes preuves en s'appuyant sur celles précédemment définies pour Frama-C. 

\subsection{Saisie}

Enfin, le dernier problème rencontré réside dans la compréhension des saisies à réaliser: constantes, variables, sets, actions... Nous avons donc passé du temps à analyser la documentation pour tenter de les comprendre. 

\clearpage

\listoffigures

\appendix
\part*{Annexes}
\addcontentsline{toc}{part}{Annexes}

\chapter{Capture écran - TLA Toolbox - Algorithme 1 : Paramètres}
\label{annexe:diagramme}
\begin{center}
	\vspace*{-0.7in}
	\includegraphics[angle=90,scale=0.45]{figures/tla_algo1_params}
\end{center}

\chapter{Capture écran - TLA Toolbox - Algorithme 1 : Execution}
\label{annexe:diagramme}
\begin{center}
	\vspace*{-0.7in}
	\includegraphics[angle=90,scale=0.45]{figures/tla_algo1_exec}
\end{center}

\chapter{Capture écran - TLA Toolbox - Algorithme 2 : Paramètres}
\label{annexe:diagramme}
\begin{center}
	\vspace*{-0.7in}
	\includegraphics[angle=90,scale=0.45]{figures/tla_algo2_params}
\end{center}

\chapter{Capture écran - TLA Toolbox - Algorithme 2 : Execution}
\label{annexe:diagramme}
\begin{center}
	\vspace*{-0.7in}
	\includegraphics[angle=90,scale=0.45]{figures/tla_algo2_exec}
\end{center}

\chapter{Frama-C - Algorithme 1}

\begin{center}
	\includegraphics[scale=0.7]{figures/framac_algo1_1}
\end{center}

\begin{center}
	\includegraphics[scale=0.7]{figures/framac_algo1_2}
\end{center}

\chapter{Frama-C - Algorithme 2}

\begin{center}
	\includegraphics[scale=0.7]{figures/framac_algo2_1}
\end{center}

\begin{center}
	\includegraphics[scale=0.685]{figures/framac_algo2_2}
\end{center}

\begin{center}
	\includegraphics[scale=0.72]{figures/framac_algo2_3}
\end{center}

\chapter{Rodin - Algorithme 1 : Contexte}

\begin{center}
	\includegraphics[scale=0.7]{figures/rodin_algo1_context}
\end{center}

\chapter{Rodin - Algorithme 1 : Machine}

\begin{center}
	\includegraphics[scale=0.7]{figures/rodin_algo1_machine_1}
\end{center}

\begin{center}
	\includegraphics[scale=0.7]{figures/rodin_algo1_machine_2}
\end{center}

\chapter{Rodin - Algorithme 2 : Contexte}

\begin{center}
	\includegraphics[scale=0.7]{figures/rodin_algo2_context}
\end{center}

\chapter{Rodin - Algorithme 2 : Machine}

\begin{center}
	\includegraphics[scale=0.65]{figures/rodin_algo2_machine_1}
\end{center}

\begin{center}
	\includegraphics[scale=0.7]{figures/rodin_algo2_machine_2}
\end{center}

\end{document}
