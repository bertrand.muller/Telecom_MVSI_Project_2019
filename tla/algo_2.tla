----------------------------- MODULE algo_2 -----------------------------
EXTENDS TLC, Integers, Naturals, Sequences
CONSTANTS arrayParam

(* --algorithm tri_selection {
    variables array, c, d, position, swap;
{
    array := arrayParam;
   
    c := 1;
    while ( c < Len(array))
    {
        position := c;
    
        d := c+1;
        while ( d <= Len(array) )
        {
            if ( array[position] > array[d] )
                position := d;
            d := d+1;
        };
        if ( position /= c )
        {
            swap := array[c];
            array[c] := array[position];
            array[position] := swap;
        };
        c := c+1;
    };
    
    print <<"Sorted list in ascending order:">>;
    
    c := 1;
    while ( c <= Len(array) )
    {
        print <<array[c]>>;
        c := c+1;
    };
}
}
*)
\* BEGIN TRANSLATION
CONSTANT defaultInitValue
VARIABLES array, c, d, position, swap, pc

vars == << array, c, d, position, swap, pc >>

Init == (* Global variables *)
        /\ array = arrayParam
        /\ c = 1
        /\ d = defaultInitValue
        /\ position = 1
        /\ swap = defaultInitValue
        /\ pc = "Lbl_1"

Lbl_1 == /\ pc = "Lbl_1"
         /\ array' = arrayParam
         /\ c' = 1
         /\ pc' = "Lbl_2"
         /\ UNCHANGED << d, position, swap >>

Lbl_2 == /\ pc = "Lbl_2"
         /\ IF c < Len(array)
               THEN /\ position' = c
                    /\ d' = c+1
                    /\ pc' = "Lbl_3"
                    /\ c' = c
               ELSE /\ PrintT(<<"Sorted list in ascending order:">>)
                    /\ c' = 1
                    /\ pc' = "Lbl_6"
                    /\ UNCHANGED << d, position >>
         /\ UNCHANGED << array, swap >>

Lbl_3 == /\ pc = "Lbl_3"
         /\ IF d <= Len(array)
               THEN /\ IF array[position] > array[d]
                          THEN /\ position' = d
                          ELSE /\ TRUE
                               /\ UNCHANGED position
                    /\ d' = d+1
                    /\ pc' = "Lbl_3"
                    /\ UNCHANGED << array, swap >>
               ELSE /\ IF position /= c
                          THEN /\ swap' = array[c]
                               /\ array' = [array EXCEPT ![c] = array[position]]
                               /\ pc' = "Lbl_4"
                          ELSE /\ pc' = "Lbl_5"
                               /\ UNCHANGED << array, swap >>
                    /\ UNCHANGED << d, position >>
         /\ c' = c

Lbl_4 == /\ pc = "Lbl_4"
         /\ array' = [array EXCEPT ![position] = swap]
         /\ pc' = "Lbl_5"
         /\ UNCHANGED << c, d, position, swap >>

Lbl_5 == /\ pc = "Lbl_5"
         /\ c' = c+1
         /\ pc' = "Lbl_2"
         /\ UNCHANGED << array, d, position, swap >>

Lbl_6 == /\ pc = "Lbl_6"
         /\ IF c <= Len(array)
               THEN /\ PrintT(<<array[c]>>)
                    /\ c' = c+1
                    /\ pc' = "Lbl_6"
               ELSE /\ pc' = "Done"
                    /\ c' = c
         /\ UNCHANGED << array, d, position, swap >>

Next == Lbl_1 \/ Lbl_2 \/ Lbl_3 \/ Lbl_4 \/ Lbl_5 \/ Lbl_6
           \/ (* Disjunct to prevent deadlock on termination *)
              (pc = "Done" /\ UNCHANGED vars)

Spec == Init /\ [][Next]_vars

Termination == <>(pc = "Done")

\* END TRANSLATION

NoErrorAtExecution == pc="Done" => \A q \in 1..Len(array)-1: array[q] \leq array[q+1]

(* c et position sont dans les bornes du tableau*)
Invariant == 
    /\ c \leq Len(array)+1
    /\ c \geq 0
    /\ position \leq Len(array)
    /\ position \geq 0


==========
