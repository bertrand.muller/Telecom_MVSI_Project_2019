----------------------------- MODULE algo_1 -----------------------------
EXTENDS TLC, Integers, Naturals
CONSTANTS n

(*
--algorithm programme_premier {
    variables i=3, count, c, break;
{        
    if ( n >= 1 )
    {        
        print <<"First", n, "prime numbers are :">>;
        print <<2>>;
    };
    count := 2;
    while(count <= n)
    {
        c := 2;
        break := FALSE;
        while( c < i /\ \neg break)
        {
            if ( i % c  = 0 )
            {
                c := c-1;
                break := TRUE;
            };
            c := c+1;
        };
        if ( c = i )
        {
            print <<i>>;
            count := count+1;
        };
        i := i+1;
    };
}
}
*)
\* BEGIN TRANSLATION
CONSTANT defaultInitValue
VARIABLES i, count, c, break, pc

vars == << i, count, c, break, pc >>

Init == (* Global variables *)
        /\ i = 3
        /\ count = defaultInitValue
        /\ c = defaultInitValue
        /\ break = defaultInitValue
        /\ pc = "Lbl_1"

Lbl_1 == /\ pc = "Lbl_1"
         /\ IF n >= 1
               THEN /\ PrintT(<<"First", n, "prime numbers are :">>)
                    /\ PrintT(<<2>>)
               ELSE /\ TRUE
         /\ count' = 2
         /\ pc' = "Lbl_2"
         /\ UNCHANGED << i, c, break >>

Lbl_2 == /\ pc = "Lbl_2"
         /\ IF count <= n
               THEN /\ c' = 2
                    /\ break' = FALSE
                    /\ pc' = "Lbl_3"
               ELSE /\ pc' = "Done"
                    /\ UNCHANGED << c, break >>
         /\ UNCHANGED << i, count >>

Lbl_3 == /\ pc = "Lbl_3"
         /\ IF c < i /\ \neg break
               THEN /\ IF i % c  = 0
                          THEN /\ c' = c-1
                               /\ break' = TRUE
                          ELSE /\ TRUE
                               /\ UNCHANGED << c, break >>
                    /\ pc' = "Lbl_4"
                    /\ UNCHANGED << i, count >>
               ELSE /\ IF c = i
                          THEN /\ PrintT(<<i>>)
                               /\ count' = count+1
                          ELSE /\ TRUE
                               /\ count' = count
                    /\ i' = i+1
                    /\ pc' = "Lbl_2"
                    /\ UNCHANGED << c, break >>

Lbl_4 == /\ pc = "Lbl_4"
         /\ c' = c+1
         /\ pc' = "Lbl_3"
         /\ UNCHANGED << i, count, break >>

Next == Lbl_1 \/ Lbl_2 \/ Lbl_3 \/ Lbl_4
           \/ (* Disjunct to prevent deadlock on termination *)
              (pc = "Done" /\ UNCHANGED vars)

Spec == Init /\ [][Next]_vars

Termination == <>(pc = "Done")

\* END TRANSLATION

NoErrorAtExecution == pc = "Done" => count-1 = n

==========
